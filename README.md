<p align="center">
<h1 align="center"> TodoList App Backend</h1>
</p>


This application allows you to create and manage to-do lists using a database.


## Installation

 First of all, Mysql connection should be made.

1- Once you have installed MySQL, you can create the database that you will use in this tutorial. Log into the database using the MySQL client using the login details of the database administrator.
```bash
$ mysql -u root -p
```

2- Create a new database and switch into it by using the following command.
```bash
$ create database todolistapp;
$ use todolistapp;
```

3- Next, create a user associated with this database. It is always a good idea to have separate users for each database on the system. The following commands will create the user and grant them all permissions on the todolistapp database.

```bash
$ create user 'todousers'@'localhost' identified by 'admin';
$ grant all on todolistapp.* to 'todousers'@'localhost';
```

4- Create the todos table in the new database.

```bash
create table todos(
  id INT AUTO_INCREMENT,
  owner VARCHAR(255) NOT NULL,
  name VARCHAR(255) NOT NULL,
  isChecked BOOLEAN,
  PRIMARY KEY (id),
  INDEX (owner)
);
```

5-  Make MySql connection settings to access the database.
![Mysql setting](./Mysql.PNG)

 
6- Run this command to make the necessary libraries and installations.

```bash
$ npm install
```

7- Run `node src/index.js` for a backend server.
```bash
$ node src/index.js
```




