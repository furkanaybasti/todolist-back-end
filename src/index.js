const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser');
const mysql = require('mysql');
const todos = require('./todos');
const bearerToken = require('express-bearer-token');
const oktaAuth = require('./auth');

const connection = mysql.createConnection({
  host     : 'localhost',
  user     : 'todousers',
  password : 'admin',
  database : 'todolistapp'
});

connection.connect();

const port = process.env.PORT || 8080;

const app = express()
  .use(cors())
  .use(bodyParser.json())
  .use(bearerToken())
  .use(oktaAuth)
  .use(todos(connection));

app.listen(port, () => {
  console.log(`Express server listening on port ${port}`);
});
