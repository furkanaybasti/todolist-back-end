const express = require('express');

function createRouter(db) {
  const router = express.Router();

  router.post('/todos', (req, res, next) => {
    const owner = req.user.email;
    db.query(
      'INSERT INTO todos (owner, name,isChecked) VALUES (?,?,?)',
      [owner, req.body.name, req.body.isChecked],
      (error) => {
        if (error) {
          console.error(error);
          res.status(500).json({ status: 'error' });
        } else {
          res.status(200).json({ status: 'ok' });
        }
      }
    );
  });

  router.get('/todos', function (req, res, next) {
    const owner = req.user.email;
    db.query(
      'SELECT id, name, isChecked FROM todos WHERE owner=? ORDER BY id',
      [owner, 10 * (req.params.page || 0)],
      (error, results) => {
        if (error) {
          console.log(error);
          res.status(500).json({ status: 'error' });
        } else {
          res.status(200).json(results);
        }
      }
    );
  });

  router.put('/todos/:id', function (req, res, next) {
    const owner = req.user.email;
    db.query(
      'UPDATE todos SET name=? WHERE id=? AND owner=?',
      [req.body.name, req.params.id, owner],
      (error) => {
        if (error) {
          res.status(500).json({ status: 'error' });
        } else {
          res.status(200).json({ status: 'ok' });
        }
      }
    );
  });

  router.put('/altertodos/:id', function (req, res, next) {
    const owner = req.user.email;
    db.query(
      'UPDATE todos SET isChecked=? WHERE id=? AND owner=?',
      [req.body.isChecked, req.params.id, owner],
      (error) => {
        if (error) {
          res.status(500).json({ status: 'error' });
        } else {
          res.status(200).json({ status: 'ok' });
        }
      }
    );
  });

  router.delete('/todos/:id', function (req, res, next) {
    const owner = req.user.email;
    db.query(
      'DELETE FROM todos WHERE id=? AND owner=?',
      [req.params.id, owner],
      (error) => {
        if (error) {
          res.status(500).json({ status: 'error' });
        } else {
          res.status(200).json({ status: 'ok' });
        }
      }
    );
  });

  return router;
}

module.exports = createRouter;
